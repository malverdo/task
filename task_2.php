<?php
// Задание 2
// Спроектируйте структуру базы данных MySQL/PostgreSQL для библиотеки, удовлетворяющую следующим требованиям:

// необходимо хранить только авторов и книги;
// книга может быть написана в соавторстве (несколькими авторами);
// автор за свою жизнь может написать несколько книг.

// Напишите SQL-запрос, который выведет авторов, написавших не более 6 книг.


"CREATE TABLE books(
	id INT NOT NULL AUTO_INCREMENT,
    book VARCHAR(120),
    PRIMARY KEY (id)
)";

"CREATE TABLE autor(
	id INT NOT NULL AUTO_INCREMENT,
    name_autor VARCHAR(120),
    book_id INT,
    PRIMARY KEY (id),
    FOREIGN KEY(book_id) REFERENCES books(id) 
)";

"INSERT INTO `books`( `book`) VALUES
    ('Мастер и Маргарита'),('Евгений Онегин'),('Преступление и наказание'),('Война и мир'),
    ('аленький принц'),('Герой нашего времени'),('Двенадцать стульев'),('1984'),('Сто лет одиночества'),
    ('Гарри Поттер и философский камень'),('Мёртвые души'),('Анна Каренина'),('Идиот'),('Портрет Дориана Грея'),
    ('Горе от ума'),('Отцы и дети'),('Властелин колец'),('Над пропастью во ржи')";


// добавить авторы и id 
"INSERT INTO `autor`(`name_autor`, `book_id`) VALUES
    (Булгаков ,3) , ('Булгаков' , 17) , ('Булгаков' ,16) ,('Булгаков' , 4) ,('Булгаков' , 7) , ('Булгаков' , 9) , ('Булгаков', 11)  , ('Булгаков' , 14),
    ('Марлина РО' , 3) , ('Мойн Де Волд' , 2) ,('Шан хан' , 7) ,('Толстой' , 7) , ('Роббин.М.С ', 8) , ('Волд.С.А ', 12)  , ('Романов.А.С', 15)
";

//вывод таблиц  
"SELECT autor.name_autor ,books.book FROM `books` INNER JOIN `autor` ON books.id = autor.book_id";

// вывод авторов написавших не более 6 книг
"SELECT name_autor, COUNT(*) as count FROM autor GROUP BY name_autor HAVING count<=6";


